import {test} from 'playwright-elements';
import dotenv from 'dotenv';
import filterValues from "../common/types/filter";
import {MainPage} from "../web-ui/pages/main.page";
import {PerfumePage} from "../web-ui/pages/perfume.page";
import {HeaderMenu} from "../common/enums/header-menu";

dotenv.config();
const mainPage = new MainPage();
const perfumePage = new PerfumePage();

for (const {filters} of filterValues) {
  test(`should allow me to filter perfumes: ${filters.map(filter => filter.filterName + ": " + filter.filterValue + " ")}`,
      async ({goto}) => {
    await goto(process.env.BASE_URL);
    await mainPage.cookiesPopup.click();
    await mainPage.headerComponent.openMenuItem(HeaderMenu.PARFUM);
    for (const test of filters) {
      await perfumePage.filterComponent.setFilter(test.filterName, test.filterValue);
    }
    for (const test of filters) {
      await perfumePage.searchResultComponent.verifySearchResult(test.filterName, test.filterValue);
    }
  });
}