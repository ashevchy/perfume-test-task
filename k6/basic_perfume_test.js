import { check } from 'k6';
import http from 'k6/http';
import {testOptions} from "./options.js";

export const options = testOptions;

export default function () {

  const url = "https://www.douglas.de/de/c/parfum/01?q=:relevance:flags:computedLimited:brand:b0061:Geschenk+f%C3%BCr:Geburtstag:classificationClassName:Duftset:gender:UNISEX";
  const params = {
    headers: {},
  };
  const res = http.get(url, params);
  check(res, {
    "Get Perfume data: is 200": (r) => r.status === 200,
  });
}