import { uuidv4 } from 'https://jslib.k6.io/k6-utils/1.4.0/index.js';
export const testOptions = {
    tags: {
        testid: uuidv4(),
    },
    scenarios: {
        filter_perfume: {
            executor: 'ramping-vus',
            startVUs: 0,
            stages: [
                { duration: `10s`, target: 10 },
                { duration: `10s`, target: 10 },
                { duration: `10s`, target: 0 }
            ],
            gracefulRampDown: '10s',
            gracefulStop: '10s'
        },
    },
};