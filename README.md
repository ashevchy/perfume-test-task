I have diligently followed the provided instructions and successfully accomplished the specified tasks:

Navigated to the website https://www.douglas.de/de.
Handled the cookie consent promptly to ensure a seamless user experience.
Clicked on the "Parfum" category to access the perfume section.

Implemented data-driven tests to list products based on filters. The tests covered various criteria, including Marke, Produktart, Geschenk fur, Fur Wen, Sale, Neu, and Limitiert. I thoroughly tested different values to ensure the correct display of products and accounted for all possible combinations and scenarios.

Playwright Test Execution:

`npx playwright test --config=web-ui/playwright.config.ts`

Simple playwright report:
![Playwrite_Report.png](images%2FPlaywrite_Report.png)
![Playwrite_Report_2.png](images%2FPlaywrite_Report_2.png)

To assess the website's performance, I extended the task to collect baseline performance metrics, as requested. For this purpose, I utilized the K6 tool, configuring it to simulate a load of 10 users accessing the URL https://www.douglas.de/de/c/parfum/01?q=:relevance:flags:computedLimited:brand:b0061:Geschenk+f%C3%BCr:Geburtstag:classificationClassName:Duftset:gender:UNISEX. The test was executed in three stages:

K6 Test Execution:

`k6 run k6/basic_perfume_test.js`

Stage 1:
·We started with 0 users and ramped up to 10 users in a 10-second duration, each user continuously executing a GET request for the provided URL.

Stage 2:

· In a 10-second duration, all 10 users simultaneously accessed the URL.

Stage 3:

·We ramped down the number of users from 10 to 0 over 10 seconds while still executing GET requests for the provided URL.

After the test, we can check key performance metrics such as http_req_duration, http_req_failed, and http_reqs from the console.
![Console_logs.png](images%2FConsole_logs.png)
For more comprehensive logging and analysis, I also configured Prometheus to collect and store metrics, while Grafana was used to visualize and display the data. Grafana provides valuable insights into the performance metrics, enabling a better understanding and potential areas for optimization.
![Grafana_1.png](images%2FGrafana_1.png)
![Grafana_2.png](images%2FGrafana_2.png)
![Grafana_3.png](images%2FGrafana_3.png)