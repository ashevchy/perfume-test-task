import { PlaywrightTestConfig } from '@playwright/test';

const config: PlaywrightTestConfig = {
  expect: { timeout: 5_000 },
  timeout: 6_000_0,
  reporter: [
    ['html', {
      open: 'never',
      outputFolder:'../test-results',
    }],
  ],
  projects: [
    {
      name: 'Chrome',
      use: {
        browserName: 'chromium',
        channel:  'chrome',
        launchOptions: { slowMo: 1000 },
        viewport: {
          width: 1920,
          height: 1080,
        },
      },
    },
  ],
  testDir: '../tests',
};

export default config;