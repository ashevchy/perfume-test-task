
import {FilterComponent} from "./filter.component";
import {SearchResultComponent} from "./search-result.component";

export class PerfumePage {
  public readonly filterComponent = new FilterComponent();
  public readonly searchResultComponent = new SearchResultComponent();
}