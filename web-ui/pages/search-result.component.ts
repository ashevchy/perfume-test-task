import {$, expect} from 'playwright-elements';
import {FiltersNames} from "../../common/enums/filters";

export class SearchResultComponent {
  readonly searchResultItems = $('#productlisting')
  .subElements({
    brandName: $("div.top-brand"),
    categoryName: $("div.category")
  });

  async verifySearchResult(filterName: string, filterValue: string){
    if (filterName.startsWith(FiltersNames.Marke)){
      await this.verifyBrand(filterValue);
    }
    if(filterName.startsWith(FiltersNames.Produktart)) {
      await this.verifyCategory(filterValue);
    }
  }

 private async verifyBrand(brandName: string) {
    this.searchResultItems.brandName.hasText(brandName)
  }

 private async verifyCategory(categoryName: string) {
    this.searchResultItems.categoryName.hasText(categoryName);
  }
}