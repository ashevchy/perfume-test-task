import {$} from 'playwright-elements';

export class FilterComponent {
  readonly filterItem = $('div.facet');

  async setFilter(filterName: string, filterValue: string) {
    const filter = this.filterItem.filter({hasText: filterName});
    await filter.click();
    await filter.$getByRole('checkbox', { name: filterValue }).click();
  }
}