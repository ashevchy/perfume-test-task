import {$} from 'playwright-elements';
import {HeaderComponent} from "./header.component";

export class MainPage {
  public readonly headerComponent = new HeaderComponent();
  readonly cookiesPopup = $('button.uc-list-button__accept-all')
}