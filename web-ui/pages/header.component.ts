import {$} from 'playwright-elements';

export class HeaderComponent {
  readonly headerMenu = $('div.navigation-main__container').subElements({
    menus: $("a.link--nav-heading")
  })

  async openMenuItem(menuName: string) {
    await this.headerMenu.menus.filter({hasText: menuName}).click();
  }
}