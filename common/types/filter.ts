import {FiltersNames} from "../enums/filters";

type FilterInfo = {
  filterName: string;
  filterValue: string;
};

type Filters = {
  filters: FilterInfo[];
}

const filterValues: Filters[] = [
  {
    filters: [
      {
        filterName: FiltersNames.Marke,
        filterValue: "100BON",
      },
      {
        filterName: FiltersNames.Produktart,
        filterValue: "Parfum"
      },
      {
        filterName: FiltersNames.FurWen,
        filterValue: "Unisex"
      }
    ]
  },
  {
    filters: [
      {
        filterName: FiltersNames.Produktart,
        filterValue: "After Shave"
      },
      {
        filterName: FiltersNames.FurWen,
        filterValue: "Männlich"
      }
    ]
  },
  {
    filters: [
      {
        filterName: FiltersNames.Marke,
        filterValue: "Carolina Herrera",
      },
      {
        filterName: FiltersNames.Produktart,
        filterValue: "Duftset"
      },
      {
        filterName: FiltersNames.Geschenkfur,
        filterValue: "Geburtstag"
      },
      {
        filterName: FiltersNames.FurWen,
        filterValue: "Unisex"
      }
    ]
  }
]
export default filterValues;